# Extreme Tuxracer for Ubuntu Touch

This is a modified fork of [pseuudonym404](https://github.com/pseuudonym404/tuxracer-touch).

All credits for auto-compiling SDL libraries go to [zub](https://gitlab.com/zub2/SDLDemo).

Thic project combines them all, moves original pseuudonym404's Tux Racer compilation process from make to cmake and uses clickable for building.

## Prerequisites
Before you start building the project you must obtain compatible SDL sources. More info on how this works can be found on [David Kozub's sdl-demo project](https://gitlab.com/zub2/SDLDemo). However, for the means of this project it is enough to do the following:

1. install mercurial and git packages: `apt install mercurial git`
1. Open a terminal, go to the root of the project and run `ext/download-dependencies.sh`.

## Building
Run `clickable` or alternatively check out the [clickable documentation](http://clickable.bhdouglass.com/en/latest/commands.html) for further information.

## Running on a phone
The final click package can be installed and launched on a phone. However although sound and touch seems to work the image is broken and currently nothing can be seen on the screen.

### Dependency issue
The current project has a bit hacky dependency list as library required by this project can't be installed with latest packages available in Ubuntu repositories. The package that breaks this is `libgles1-mesa-dev:armhf` and there is a version number mismatch.

1. `libgles1-mesa-dev:armhf`

```
The following packages have unmet dependencies:
 libgles1-mesa-dev:armhf : Depends: libgles1-mesa:armhf (= 12.0.6-0ubuntu0.16.04.1)
```

2. `libgles1-mesa:armhf`

```
The following packages have unmet dependencies:
 libgles1-mesa:armhf : Depends: libglapi-mesa:armhf (= 11.2.1+0ubports1+0~20180119050819.4~1.gbp59218d) but 17.2.8-0ubuntu0~16.04.1 is to be installed
```
